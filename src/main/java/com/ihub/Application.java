package com.ihub;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ihub.config.AppConfiguration;
import com.ihub.config.SwaggerConfig;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = { "com.ihub" })
@EnableJpaRepositories(basePackages = "com.ihub.repositories")
@EnableTransactionManagement
@EntityScan(basePackages = "com.ihub.entities")
public class Application {

	public static void main(String[] args) {
	     new SpringApplicationBuilder(
	    		 Application.class,
        		 AppConfiguration.class,
        		 SwaggerConfig.class).run(args);

	}

}
