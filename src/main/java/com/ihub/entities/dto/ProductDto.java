package com.ihub.entities.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

	@Column(length = 50)
	@NotBlank(message = "Name is required")
	private String name;

	@Column(length = 50)
	@NotBlank(message = "Price is required")
	private String price;

	@ApiModelProperty(dataType = "java.lang.String", example = "10/10/1985")
	@NotBlank(message = "Date is required")
	@Column
	private String date;
	
}