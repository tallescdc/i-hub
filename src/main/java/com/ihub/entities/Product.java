package com.ihub.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(length = 50)
	@NotBlank(message = "Name is required")
	private String name;

	@Column(length = 50)
	@NotBlank(message = "Price is required")
	private String price;

	@ApiModelProperty(dataType = "java.lang.String", example = "10/10/1985")
	@NotBlank(message = "Date is required")
	@Column
	private String date;

}
