package com.ihub.util;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ResponseIHUB<T> {

    @ApiModelProperty(
            value = "response content",
            allowableValues = "true,false",
            required = true,
            readOnly = true,
            dataType = "boolean"
    )
    private boolean valid = true;
    
    
    
    @ApiModelProperty(
            value = "content",
            allowableValues = "content",
            dataType = "List<String>"
    )
     
    private T content;
    
    
    
    @ApiModelProperty(
            value = "code",
            allowableValues = "code",
            readOnly = true,
            dataType = "String"
    )
    private String code;
    
    
    
    @ApiModelProperty(
            value = "message",
            allowableValues = "message",
            readOnly = true,
            dataType = "List<String>"
    )
    private List<String> messages;


    @ApiModelProperty(
            value = "Status HTTP of response",
            dataType = "String"
    )
    private String status;

   
    @ApiModelProperty(
            value = "date",
            dataType = "OffSetDateTime"
    )
    private OffsetDateTime timestamp;

    public static <E> ResponseIHUB<E> validateContent(E conteudo){
        return ResponseIHUB.<E>builder()
                .content(conteudo)
                .status(HttpStatus.OK.getReasonPhrase())
                .code(HttpStatus.OK.toString())
                .timestamp(OffsetDateTime.now())
                .valid(true)
                .build();
    }

    public static <E> ResponseIHUB<E> validateContentEmpty(){
        return ResponseIHUB.<E>builder()
                .status(HttpStatus.OK.getReasonPhrase())
                .code(HttpStatus.OK.toString())
                .timestamp(OffsetDateTime.now())
                .valid(true)
                .build();
    }

    
    public ResponseIHUB(T content) {
        this.valid = true;
        this.content = content;
    }

    public void validade() {
        this.valid = false;
    }

	public void putMessage(String mensagem) {
		if (this.messages == null) {
			this.messages = new ArrayList<>();
		}
		this.messages.add(mensagem);
	}

    /**
     * Adiciona uma mensagem e inválida o status
     *
     * @param erro Erros
     */
    public void addErro(String erro) {
    	validade();
        putMessage(erro);
    }

    /**
     * Adiciona uma Lista de Mensagens de erro e invalida o status
     *
     * @param erros Erros
     */
    public void addErros(List<String> erros) {
    	validade();
        erros.forEach(this::putMessage);
    }
}