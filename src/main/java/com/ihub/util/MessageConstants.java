package com.ihub.util;

public interface MessageConstants {

	public static String SUCCESS_MESSAGE = "Record saved with sucess !!!";
	
	public static String DELETE_MESSAGE = "Record deleted with sucess !!!";
	
	public static String EMPTY = " No records !!!";

	public static String CODE_200 = "200";

	public static String OK = "OK";

	public static String CREATED = "created";
	
	public static String STATUS = "getStatus";
	
	public static String UPDATE = "updatedProduct";
	
	public static String LIST = "listProduct";
	
	public static String GET = "getProduct";
	
	public static String USER_TEST = "Talles";
}
