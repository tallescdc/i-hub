package com.ihub.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;


@ResponseStatus(value = HttpStatus.OK)
public final class IHUBException extends BusinessException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3859123020043625501L;

	public IHUBException(String mensagem, String msg) {
        super(mensagem, msg);
    }

    public IHUBException(String mensagem, List<String> msgs) {
        super(mensagem, msgs);
    }

    public IHUBException(String mensagem, Throwable cause, String msg) {
        super(mensagem, cause, msg);
    }

    public IHUBException(String mensagem, Throwable cause, List<String> msgs) {
        super(mensagem, cause, msgs);
    }
}
