package com.ihub.util;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import lombok.Getter;

@Getter
public abstract class BusinessException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5717792309678250681L;
	private final List<String> list;
	private final OffsetDateTime timestamp = OffsetDateTime.now();

	public BusinessException(String mensagem, String motivo) {
		super(mensagem);
		this.list = Collections.unmodifiableList(Collections.singletonList(motivo));
	}

	public BusinessException(String mensagem, List<String> motivos) {
		super(mensagem);
		this.list = Collections.unmodifiableList(motivos);
	}

	public BusinessException(String mensagem, Throwable cause, String motivo) {
		super(mensagem, cause);
		this.list = Collections.unmodifiableList(Collections.singletonList(motivo));
	}

	public BusinessException(String mensagem, Throwable cause, List<String> motivos) {
		super(mensagem, cause);
		this.list = Collections.unmodifiableList(motivos);
	}

	public String getMensagem() {
		return super.getMessage();
	}
}
