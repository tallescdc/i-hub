package com.ihub.controllers;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ihub.entities.Product;
import com.ihub.entities.dto.ProductDto;
import com.ihub.repositories.ProductRepository;
import com.ihub.util.MessageConstants;
import com.ihub.util.ResponseIHUB;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("Product")
@Api(value = "Product API", tags = "Product")
public class ProductController implements MessageConstants {

	private List<String> messages = new ArrayList<String>();
	private final ProductRepository productRepository;

	@Autowired
	public ProductController(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@ApiOperation("List of Products")
	@GetMapping(value = "list", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseIHUB<List<Product>> listar() {

		List<Product> products = (List<Product>) productRepository.findAll();

		if (products.isEmpty()) {
			sendMessageAndLog(EMPTY);
			products = null;
		} else {
			sendMessageAndLog(LIST);
		}

		return ResponseIHUB.<List<Product>>builder().messages(messages).content(products).valid(true).code(CODE_200)
				.status(OK).timestamp(OffsetDateTime.now()).build();
	}

	@ApiOperation("Create Product")
	@PostMapping("/create")
	public ResponseIHUB<Product> create(@Valid ProductDto productDto, BindingResult result, Model model) {
		
		Product product = new Product();
		product.setDate(productDto.getDate());
		product.setName(productDto.getName());
		product.setPrice(productDto.getPrice());

		productRepository.save(product);
		if (product != null) {
			sendMessageAndLog(SUCCESS_MESSAGE);
		}

		return ResponseIHUB.<Product>builder().messages(messages).content(product).valid(true).code(CODE_200).status(OK)
				.timestamp(OffsetDateTime.now()).build();

	}

	@ApiOperation("Update Product")
	@PutMapping("/update/{id}")
	public ResponseIHUB<Product> update(@PathVariable("id") long id, @RequestBody ProductDto productDto) {

		Product oldProduct = productRepository.findById(id);

		if (oldProduct != null) {
			oldProduct.setDate(productDto.getDate());
			oldProduct.setName(productDto.getName());
			oldProduct.setPrice(productDto.getPrice());
			productRepository.save(oldProduct);
			sendMessageAndLog(SUCCESS_MESSAGE);
		} else {
			sendMessageAndLog(EMPTY);
			productDto = new ProductDto();
		}

		return ResponseIHUB.<Product>builder().messages(messages).content(oldProduct).valid(true).code(CODE_200).status(OK)
				.timestamp(OffsetDateTime.now()).build();

	}

	@ApiOperation("Delete Product")
	@GetMapping("/delete/{id}")
	public ResponseIHUB<Product> delete(@PathVariable("id") long id, Model model) {
		Product product = productRepository.findById(id);

		if (product != null) {
			productRepository.delete(product);
			sendMessageAndLog(DELETE_MESSAGE);
		} else {
			sendMessageAndLog(EMPTY);
		}

		return ResponseIHUB.<Product>builder().messages(messages).content(product).valid(true).code(CODE_200).status(OK)
				.timestamp(OffsetDateTime.now()).build();

	}

	/**
	 * Method to save log and send messages
	 * 
	 * @param product
	 */
	private List<String> sendMessageAndLog(String msg) {
		messages = new ArrayList<String>();
		log.info(msg);
		messages.add(msg);
		return messages;
	}

}
