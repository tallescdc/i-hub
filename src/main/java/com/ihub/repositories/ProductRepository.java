package com.ihub.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ihub.entities.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

	List<Product> findByName(String name);

	Product findById(long id);

}
