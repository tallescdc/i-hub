package com.ihub.test;

import java.util.Calendar;

import com.ihub.entities.Product;

public class TestBuilder {

	public static Product getProductBuilder() {
		Product product = new Product();
		product.setPrice("10.0");
		product.setName("Product i-hub-01");
		product.setDate(Calendar.getInstance().toString());
		return product;
	}

}