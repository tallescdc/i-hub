package com.ihub.test;

import static org.mockito.Mockito.mock;

import org.junit.BeforeClass;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import com.ihub.controllers.ProductController;
import com.ihub.entities.Product;
import com.ihub.repositories.ProductRepository;

import lombok.Getter;
import lombok.Setter;

public class ProductControllerUnitTest extends TestBuilder {

	@Getter
	@Setter
	private static ProductController mockedProductController;
	@Getter
	@Setter
	private static ProductRepository mockedProductRepository;
	@Getter
	@Setter
	private static BindingResult mockedBindingResult;
	@Getter
	@Setter
	private static Model mockedModel;
	@Getter
	@Setter
	private static Product product;

	@BeforeClass
	public static void setUpProductControllerInstance() {
		mockedProductRepository = mock(ProductRepository.class);
		mockedBindingResult = mock(BindingResult.class);
		mockedModel = mock(Model.class);
		mockedProductController = new ProductController(mockedProductRepository);
		setProduct(getProductBuilder());
	}

}
